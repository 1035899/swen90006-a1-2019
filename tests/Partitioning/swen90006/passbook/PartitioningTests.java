package swen90006.passbook;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;
import static org.junit.Assert.*;

public class PartitioningTests
{
    protected PassBook pb;

    //Any method annotated with "@Before" will be executed before each test,
    //allowing the tester to set up some shared resources.
    @Before public void setUp()
    {
	pb = new PassBook();
    }

    //Any method annotated with "@After" will be executed after each test,
    //allowing the tester to release any shared resources used in the setup.
    @After public void tearDown()
    {
    }

    //Any method annotation with "@Test" is executed as a test.

//------------------------------addUser Test Cases-------------------------------------

    //DuplicateUserTest Test Cases

    //Existed Username in PASSBOOK
    @Test public void DuplicateUserTest() throws WeakPassphraseException, DuplicateUserException {
        boolean DuplicateUserException = false;
        String passbookUsername = "ALexTina";
        String passbookphrase = "Abcd1234";
            try{
                pb.addUser(passbookUsername, passbookphrase);
                pb.addUser(passbookUsername, passbookphrase);
            }
            catch(DuplicateUserException e){
                DuplicateUserException =  true;
                }
        assertTrue (DuplicateUserException) ;

    }

 

    //WeakPassphraseException Test cases

    //0 < Passphrase < 8
    @Test public void WeakPassphraseException() throws DuplicateUserException{
        boolean WeakPassphraseException = false;
        String passbookUsername = "AlexTina";
        String passbookphrase = "Ab23333";
        try{
            pb.addUser(passbookUsername,passbookphrase);
        }
        catch (WeakPassphraseException e){
            WeakPassphraseException = true;
        }
        assertTrue(WeakPassphraseException);
    }

   
    //passphrase.length >=8 not containsLowerCase
    @Test public void NocontainLowerCase() throws DuplicateUserException {
        boolean WeakPassphraseException = false;
    	String passbookUsername = "AlexTina";
        String passbookphrase = "ABCZ0239";
        try {
        	pb.addUser(passbookUsername, passbookphrase);
        }
        catch(WeakPassphraseException e) {
        	WeakPassphraseException = true;
        }
        assertTrue(WeakPassphraseException);
    }
    
    //passphrase.length >=8 not containsUpperCase
    @Test public void NocontainUpperCase() throws DuplicateUserException {
        boolean WeakPassphraseException = false;
    	String passbookUsername = "AlexTina";
        String passbookphrase = "abcz0239";
        try {
        	pb.addUser(passbookUsername, passbookphrase);
        }
        catch(WeakPassphraseException e) {
        	WeakPassphraseException = true;
        }
        assertTrue(WeakPassphraseException);
    }
    
  //passphrase.length >=8 not containsNumCase
    @Test public void NocontainNumCase() throws DuplicateUserException {
        boolean WeakPassphraseException = false;
    	String passbookUsername = "AlexTina";
        String passbookphrase = "abczABCZ";
        try {
        	pb.addUser(passbookUsername, passbookphrase);
        }
        catch(WeakPassphraseException e) {
        	WeakPassphraseException = true;
        }
        assertTrue(WeakPassphraseException);
    }
    //passphrase.length >=8 containsNumCase
    @Test public void containNumCase() throws DuplicateUserException, swen90006.passbook.AlreadyLoggedInException, swen90006.passbook.IncorrectPassphraseException {
    	boolean NoSuchUserException = false;
    	boolean WeakPassphraseException = false;
    	String passbookUsername = "AlexTina";
        String passbookphrase = "AAAaaa000";
        String passbookUsername2 = "ALexTina";
        String passbookphrase2 = "ZZZzzz999";
        try {
        	pb.addUser(passbookUsername, passbookphrase);
        	pb.loginUser(passbookUsername, passbookphrase);
        	pb.addUser(passbookUsername2, passbookphrase2);
        	pb.loginUser(passbookUsername2, passbookphrase2);
        }
        catch(WeakPassphraseException e) {
        	WeakPassphraseException = true;
        }
        catch(NoSuchUserException e) {
        	NoSuchUserException = true;
        }
        assertFalse(WeakPassphraseException);
        assertFalse(NoSuchUserException);
    }

//------------------------------loginUser Test Cases-------------------------------------


    // user is not in passbook
    @Test public void NoSuchUserException2() throws NoSuchUserException, AlreadyLoggedInException, IncorrectPassphraseException, WeakPassphraseException, DuplicateUserException {
        boolean NoSuchUserException = false;
        String passbookUsername = "AlexTina";
        String passbookphrase = "Abc234hg";
        String passbookUsername2 = "AlesTina";
        String passbookphrase2 = "Abc234hg";
        try {
            pb.addUser(passbookUsername,passbookphrase);
            pb.loginUser(passbookUsername2,passbookphrase2);
        }
        catch (NoSuchUserException e){
            NoSuchUserException = true;
        }
        assertTrue(NoSuchUserException);
    }

 
    // user has login
    @Test public void AlreadyLoggedInException() throws NoSuchUserException, IncorrectPassphraseException, WeakPassphraseException, DuplicateUserException {
        boolean AlreadyLoggedInException = false;
         String passbookUsername = "AlexTina";
         String passbookphrase = "Abc234hg";
        
         try{
             pb.addUser(passbookUsername,passbookphrase);
             pb.loginUser(passbookUsername,passbookphrase);
             pb.loginUser(passbookUsername,passbookphrase);
         }
         catch (AlreadyLoggedInException e){
             AlreadyLoggedInException = true;
         }
         assertTrue(AlreadyLoggedInException);
     }
    
  
    //passphrase is not equale to the saved one
    @Test public void IncorrectPassphraseException() throws WeakPassphraseException, DuplicateUserException, AlreadyLoggedInException, IncorrectPassphraseException, NoSuchUserException {
        boolean IncorrectPassphraseException = false;
        String passbookUsername = "AlexTina";
        String passbookphrase = "Abc234hg";
        String passbookUsername2 = "AlexTina";
        String passbookphrase2 = "Abc23444hg";
        try {
            pb.addUser(passbookUsername,passbookphrase);
            pb.loginUser(passbookUsername2,passbookphrase2);
        }
        catch (IncorrectPassphraseException e){
            IncorrectPassphraseException = true;
        }
        assertTrue(IncorrectPassphraseException);
    }

    //passphrase is equale to the saved one
    @Test public void IncorrectPassphraseException2() throws WeakPassphraseException, DuplicateUserException, AlreadyLoggedInException, IncorrectPassphraseException, NoSuchUserException, MalformedURLException, InvalidSessionIDException {
        boolean IncorrectPassphraseException = false;
        boolean InvalidSessionIDException = false;
        String passbookUsername = "AlexTina";
        String passbookphrase = "Abc234hg";
        URL url = new URL ("http:AlexTinaAbc234hg");
        String urlUsername = "AlexTina";
        String urlPassword = "Abc234hg";
        try {
        	
            pb.addUser(passbookUsername,passbookphrase);
            pb.updateDetails( pb.loginUser(passbookUsername,passbookphrase), url, urlUsername, urlPassword);
        }
        catch (IncorrectPassphraseException e){
            IncorrectPassphraseException = true;
        }
        catch (InvalidSessionIDException e) {
        	 InvalidSessionIDException = true;
        }
        assertFalse(IncorrectPassphraseException);
        assertFalse(InvalidSessionIDException);
    }


//------------------------------updateDetails Test Cases-------------------------------------

    //InvalidSessionIDException

    // sessionID  not exist
    @Test public void InvalidSessionIDException2() throws WeakPassphraseException, DuplicateUserException, NoSuchUserException, AlreadyLoggedInException, IncorrectPassphraseException, MalformedURLException, InvalidSessionIDException {
        boolean InvalidSessionIDException = false;
        Integer sessionID = -1 ;
        URL url = new URL ("http:AlexTinaAbc234hg");
        String Username = "AlexTina";
        String Password = "Abc234hg";
        String urlUsername = "AlexTina";
        String urlPassword = "Abc234hg";
        pb.addUser(Username,Password);

        try{
            pb.updateDetails(sessionID,url,urlUsername,urlPassword);
        }catch (InvalidSessionIDException e){
            InvalidSessionIDException = true;
        }
        assertTrue(InvalidSessionIDException);

    }




    //Protocol != [http,https]

    @Test public void MalformedURLException2() throws MalformedURLException, WeakPassphraseException, DuplicateUserException, NoSuchUserException, AlreadyLoggedInException, IncorrectPassphraseException, InvalidSessionIDException {
        boolean MalformedURLException = false;
        Integer sessionID ;
        String Username = "AlexTina";
        String Password = "Abc234hg";
        String urlUsername = "AlexTina";
        String urlPassword = "Abc234hg";
        pb.addUser(Username,Password);
        sessionID = pb.loginUser(Username,Password);
        try{
            URL url = new URL ("TPP:AlexTinaAbc234hg");
            pb.updateDetails(sessionID,url,urlUsername,urlPassword);
        }catch ( MalformedURLException e){
            MalformedURLException = true;
        }
        assertTrue(MalformedURLException);
    }

    // check the urlusername or password whether are null

    //urlUsername != null && urlPassword !=null
    @Test public void checkRemove() throws WeakPassphraseException, DuplicateUserException, NoSuchUserException, AlreadyLoggedInException, IncorrectPassphraseException, InvalidSessionIDException, MalformedURLException, NoSuchURLException {
    	boolean InvalidSessionIDException = false;
    	boolean NoSuchUserException = false;
        Integer sessionID ;
        String Username = "AlexTina";
        String Password = "Abc234hg";
        String urlUsername = "AlexTina";
        String urlPassword = "Abc234hg";
        pb.addUser(Username,Password);
        sessionID = pb.loginUser(Username,Password);
        try{
            URL url = new URL ("http:AlexTinaAbc234hg");
            pb.updateDetails(sessionID,url,urlUsername,urlPassword);
            pb.retrieveDetails(sessionID,url);
        }
        catch (NoSuchURLException e){
            NoSuchUserException = true;
        }
        catch(InvalidSessionIDException e) {
        	InvalidSessionIDException = true;
        }
        assertFalse(NoSuchUserException);
        assertFalse(InvalidSessionIDException);
    }

    //urlUsername = null || urlPassword !=null
    @Test public void checkRemove2() throws WeakPassphraseException, DuplicateUserException, NoSuchUserException, AlreadyLoggedInException, IncorrectPassphraseException, InvalidSessionIDException, MalformedURLException, NoSuchURLException {
    	boolean InvalidSessionIDException = false;
    	boolean NoSuchUserException = false;
        Integer sessionID ;
        String Username = "AlexTina";
        String Password = "Abc234hg";
        String urlUsername = null;
        String urlPassword = "Abc234hg";
        pb.addUser(Username,Password);
        sessionID = pb.loginUser(Username,Password);
        try{
            URL url = new URL ("http:AlexTinaAbc234hg");
            pb.updateDetails(sessionID,url,urlUsername,urlPassword);
            pb.retrieveDetails(sessionID,url);
        }
        catch (NoSuchURLException e){
            NoSuchUserException = true;
        }
        catch(InvalidSessionIDException e) {
        	InvalidSessionIDException = true;
        }
       
        assertFalse(InvalidSessionIDException);
        assertTrue(NoSuchUserException);
    }
    //urlUsername != null || urlPassword =null
    @Test public void checkRemove3() throws WeakPassphraseException, DuplicateUserException, NoSuchUserException, AlreadyLoggedInException, IncorrectPassphraseException, InvalidSessionIDException, MalformedURLException, NoSuchURLException {
    	boolean InvalidSessionIDException = false;
    	boolean NoSuchUserException = false;
        Integer sessionID ;
        String Username = "AlexTina";
        String Password = "Abc234hg";
        String urlUsername = "AlexTina";
        String urlPassword = null;
        pb.addUser(Username,Password);
        sessionID = pb.loginUser(Username,Password);
        try{
            URL url = new URL ("http:AlexTinaAbc234hg");
            pb.updateDetails(sessionID,url,urlUsername,urlPassword);
            pb.retrieveDetails(sessionID,url);
        }
        catch (NoSuchURLException e){
            NoSuchUserException = true;
        }
        catch(InvalidSessionIDException e) {
        	InvalidSessionIDException = true;
        }

        assertFalse(InvalidSessionIDException);
        assertTrue(NoSuchUserException);
    }
    //urlUsername != null || urlPassword !=null
    @Test public void checkRemove4() throws WeakPassphraseException, DuplicateUserException, NoSuchUserException, AlreadyLoggedInException, IncorrectPassphraseException, InvalidSessionIDException, MalformedURLException, NoSuchURLException {
    	boolean InvalidSessionIDException = false;
    	boolean NoSuchUserException = false;
        Integer sessionID ;
        String Username = "AlexTina";
        String Password = "Abc234hg";
        String urlUsername = null;
        String urlPassword = null;
        pb.addUser(Username,Password);
        sessionID = pb.loginUser(Username,Password);
        try{
            URL url = new URL ("http:AlexTinaAbc234hg");
            pb.updateDetails(sessionID,url,urlUsername,urlPassword);
            pb.retrieveDetails(sessionID,url);
        }
        catch (NoSuchURLException e){
            NoSuchUserException = true;
        }
        catch(InvalidSessionIDException e) {
        	InvalidSessionIDException = true;
        }
        assertFalse(InvalidSessionIDException);
        assertTrue(NoSuchUserException);
    }

//------------------------------retrieveDetails Test Cases-------------------------------------
    //InvalidSessionIDException

  /*  // sessionID exist
    @Test public void InvalidSessionIDExceptionRE() throws WeakPassphraseException, DuplicateUserException, NoSuchUserException, AlreadyLoggedInException, IncorrectPassphraseException, MalformedURLException, NoSuchURLException {
        boolean InvalidSessionIDException = false;
        Integer sessionID ;

        String Username = "AlexTina";
        String Password = "Abc234hg";
        String urlUsername = "AlexTina";
        String urlPassword = "Abc234hg";
        pb.addUser(Username,Password);
        sessionID = pb.loginUser(Username,Password);
        try{
            URL url = new URL ("http:AlexTinaAbc234hg");
            pb.updateDetails(sessionID,url,urlUsername,urlPassword);
            pb.retrieveDetails(sessionID,url);
        }catch (InvalidSessionIDException e){
            InvalidSessionIDException = true;
        }
        assertFalse(InvalidSessionIDException);

    }
    */
    
    // sessionID  not exist
    @Test public void InvalidSessionIDExceptionRE2() throws WeakPassphraseException, DuplicateUserException, NoSuchUserException, AlreadyLoggedInException, IncorrectPassphraseException, MalformedURLException, NoSuchURLException {
        boolean InvalidSessionIDException = false;
        Integer sessionID = -1;

        String Username = "AlexTina";
        String Password = "Abc234hg";
        String urlUsername = "AlexTina";
        String urlPassword = "Abc234hg";
        pb.addUser(Username,Password);

        try{
            URL url = new URL ("http:AlexTinaAbc234hg");
            pb.updateDetails(sessionID,url,urlUsername,urlPassword);
            pb.retrieveDetails(sessionID,url);
        }catch (InvalidSessionIDException e){
            InvalidSessionIDException = true;
        }
        assertTrue(InvalidSessionIDException);

    }
    //contain http,http:
   
    @Test public void MalformedURLException3() throws MalformedURLException, WeakPassphraseException, DuplicateUserException, NoSuchUserException, AlreadyLoggedInException, IncorrectPassphraseException, InvalidSessionIDException {
        boolean MalformedURLException = false;
        Integer sessionID ;
        String Username = "AlexTina";
        String Password = "Abc234hg";
        String urlUsername = "AlexTina";
        String urlPassword = "Abc234hg";
        pb.addUser(Username,Password);
        sessionID = pb.loginUser(Username,Password);
        try{
            URL url = new URL ("TPP:AlexTinaAbc234hg");
            pb.updateDetails(sessionID,url,urlUsername,urlPassword);
        }catch ( MalformedURLException e){
            MalformedURLException = true;
        }
        assertTrue(MalformedURLException);
    }
     // NoSuchURLException
   /* //  pt!=null
    @Test public void NoSuchURLException() throws WeakPassphraseException, DuplicateUserException, NoSuchUserException, AlreadyLoggedInException, IncorrectPassphraseException, InvalidSessionIDException, MalformedURLException, NoSuchURLException {
        boolean NoSuchURLException = false;
        Integer sessionID ;
        String Username = "AlexTina";
        String Password = "Abc234hg";
        String urlUsername = "AlexTina";
        String urlPassword = "Abc234hg";
        pb.addUser(Username,Password);
        sessionID = pb.loginUser(Username,Password);
        try{
            URL url = new URL ("http:AlexTinaAbc234hg");
            pb.updateDetails(sessionID,url,urlUsername,urlPassword);
            pb.retrieveDetails(sessionID,url);
        }
        catch (NoSuchURLException e){
            NoSuchURLException = true;
        }
        assertFalse(NoSuchURLException);
    }
    */
    //pt==null
    @Test public void NoSuchURLException2() throws WeakPassphraseException, DuplicateUserException, NoSuchUserException, AlreadyLoggedInException, IncorrectPassphraseException, InvalidSessionIDException, MalformedURLException, NoSuchURLException {
        boolean NoSuchURLException = false;
        Integer sessionID ;
        String Username = "AlexTina";
        String Password = "Abc234hg";
        String urlUsername = null;
        String urlPassword = "Abc234hg";
        pb.addUser(Username,Password);
        sessionID = pb.loginUser(Username,Password);
        try{
            URL url = new URL ("http:AlexTinaAbc234hg");
            pb.updateDetails(sessionID,url,urlUsername,urlPassword);
            pb.retrieveDetails(sessionID,url);
        }
        catch (NoSuchURLException e){
            NoSuchURLException = true;
        }
        assertTrue(NoSuchURLException);
    }

    //NoSuchURLException
    // pair = null
    @Test public void NoSuchURLExceptionPair() throws WeakPassphraseException, DuplicateUserException, NoSuchUserException, AlreadyLoggedInException, IncorrectPassphraseException, InvalidSessionIDException, MalformedURLException, NoSuchURLException {
        boolean NoSuchURLException = false;
        Integer sessionID ;
        String Username = "AlexTina";
        String Password = "Abc234hg";
        String urlUsername = "AlexTina";
        String urlPassword = "Abc234hg";
        pb.addUser(Username,Password);
        sessionID = pb.loginUser(Username,Password);
        try{
            URL url = new URL ("http:AlexTinaAbc234hg");
            pb.retrieveDetails(sessionID,url);
        }
        catch (NoSuchURLException e){
            NoSuchURLException = true;
        }
        assertTrue(NoSuchURLException);
    }

    //pair != null
    @Test public void NoSuchURLExceptionPair2() throws WeakPassphraseException, DuplicateUserException, NoSuchUserException, AlreadyLoggedInException, IncorrectPassphraseException, InvalidSessionIDException, MalformedURLException, NoSuchURLException {
        boolean NoSuchURLException = false;
        Integer sessionID ;
        String Username = "AlexTina";
        String Password = "Abc234hg";
        String urlUsername = "AlexTina";
        String urlPassword = "Abc234hg";
        pb.addUser(Username,Password);
        sessionID = pb.loginUser(Username,Password);
        try{
            URL url = new URL ("http:AlexTinaAbc234hg");
            pb.updateDetails(sessionID,url,urlUsername,urlPassword);
            pb.retrieveDetails(sessionID,url);
        }
        catch (NoSuchURLException e){
            NoSuchURLException = true;
        }
        assertFalse(NoSuchURLException);
    }






}
