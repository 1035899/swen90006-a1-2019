package swen90006.passbook;

import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;
import static org.junit.Assert.*;

//By extending PartitioningTests, we inherit tests from the script
public class BoundaryTests
    extends PartitioningTests
{
    
    @Test public void ascLowerCase() throws DuplicateUserException {
    	boolean WeakPassphraseException = false;
    	String passbookUsername = "AlexTina";
        String passbookphrase = "'{A11234";
    	try {
    		pb.addUser(passbookUsername, passbookphrase);
    	}catch(WeakPassphraseException e) {
    		WeakPassphraseException = true;
    	}
    	assertTrue(WeakPassphraseException);
    }
    
    @Test public void ascUpperCase() throws DuplicateUserException {
    	boolean WeakPassphraseException = false;
    	String passbookUsername = "AlexTina";
        String passbookphrase = "@[abc234";
    	try {
    		pb.addUser(passbookUsername, passbookphrase);
    	}catch(WeakPassphraseException e) {
    		WeakPassphraseException = true;
    	}
    	assertTrue(WeakPassphraseException);
    }
    
    @Test public void ascNumCase() throws DuplicateUserException {
    	boolean WeakPassphraseException = false;
    	String passbookUsername = "AlexTina";
        String passbookphrase = ":/Abcdef";
    	try {
    		pb.addUser(passbookUsername, passbookphrase);
    	}catch(WeakPassphraseException e) {
    		WeakPassphraseException = true;
    	}
    	assertTrue(WeakPassphraseException);
    }
    
 

}
